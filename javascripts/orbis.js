var OrbisMap = function (element, options) {
    this.mapDomElement = element;
    this.tileHolderDomElement = this.mapDomElement.getElementsByClassName('orbisTileHolder')[0];
    this.zoomedTileHolderDomElement = null;

    this.zoomLevel = 4;
    this.maxTilesOnScreenX = Math.ceil(this.mapDomElement.offsetWidth / 256);
    this.maxTilesOnScreenY = Math.ceil(this.mapDomElement.offsetHeight / 256);
    this.dragStartX = 0;
    this.dragStartY = 0;
    this.viewOffsetX = 0;
    this.viewOffsetY = 0;

    this.bindEvents();
    this.redraw();
};

OrbisMap.prototype.loadTile = function (x, y) {
    var tileDomElement = document.createElement('img');
    tileDomElement.className = 'orbisTile';
    tileDomElement.src = 'http://a.tile.openstreetmap.org/' + this.zoomLevel + '/' + x + '/' + y + '.png';
    tileDomElement.style.left = (x * 256) + 'px';
    tileDomElement.style.top = (y * 256) + 'px';

    this.tileHolderDomElement.appendChild(tileDomElement);
};

OrbisMap.prototype.bindEvents = function () {
    var self = this;

    window.addEventListener('resize', function () {
        self.maxTilesOnScreenX = Math.ceil(self.mapDomElement.offsetWidth / 256);
        self.maxTilesOnScreenY = Math.ceil(self.mapDomElement.offsetHeight / 256);

        self.redraw();
    });

    var dragEvent = self.dragView.bind(self);
    self.mapDomElement.addEventListener('mousedown', function (e) {
        e.preventDefault();

        self.dragStartX = e.clientX - self.viewOffsetX;
        self.dragStartY = e.clientY - self.viewOffsetY;

        document.addEventListener('mousemove', dragEvent);
    });

    document.addEventListener('mouseup', function () {
        document.removeEventListener('mousemove', dragEvent);
    });

    var zoomEvent = self.zoomView.bind(self);
    self.mapDomElement.addEventListener('DOMMouseScroll', zoomEvent);
};

OrbisMap.prototype.dragView = function (e) {
    e.preventDefault();

    this.viewOffsetX = Math.min(0, e.clientX - this.dragStartX);
    this.viewOffsetY = Math.min(0, e.clientY - this.dragStartY);

    this.tileHolderDomElement.style.transform = 'translate(' + this.viewOffsetX + 'px,' + this.viewOffsetY + 'px)';

    this.redraw();
};

OrbisMap.prototype.zoomView = function (e) {
    e.preventDefault();

    if (e.detail < 0) {
        this.zoomLevel++;
    } else {
        this.zoomLevel--;
    }

    if (this.zoomLevel <= 1) {
        this.zoomLevel = 1;
    }

    if (this.zoomedTileHolderDomElement != null) {
        this.mapDomElement.removeChild(this.zoomedTileHolderDomElement);
    }

    this.zoomedTileHolderDomElement = this.tileHolderDomElement.cloneNode(true);
    this.zoomedTileHolderDomElement = this.mapDomElement.appendChild(this.zoomedTileHolderDomElement);
    this.zoomedTileHolderDomElement.classList.add('orbisTileHolderZoom');

    this.zoomedTileHolderDomElement.style.transformOrigin = '0 0';
    this.zoomedTileHolderDomElement.style.transform = 'scale(' + ((e.detail < 0) ? 2 : 0.5) + ',' + ((e.detail < 0) ? 2 : 0.5) + ')';

    this.redraw();
};

OrbisMap.prototype.redraw = function () {
    this.tileHolderDomElement.innerHTML = '';

    var tileIndexOffsetX = Math.ceil(Math.abs(this.viewOffsetX) / 256);
    var tileIndexOffsetY = Math.ceil(Math.abs(this.viewOffsetY) / 256);

    for (var i = tileIndexOffsetX - 1; i < this.maxTilesOnScreenX + tileIndexOffsetX; i++) {
        for (var j = tileIndexOffsetY - 1; j < this.maxTilesOnScreenY + tileIndexOffsetY; j++) {
            this.loadTile(i, j);
        }
    }
};